# Evoleq - Examples

Examples:

* [x] Simple EvoleqFX-Application running on Android using JavaFXPorts ([Details](./javafxports-sample/ReadMe.md))
* [x] FontAwesomeFX Icon Viewer ([Details](./fontawesomefx-viewer/ReadMe.md))
* [ ] Simple EvoleqFX-Application demonstrating how to intercept shutdown process with a confirmation dialog  
* [ ] Simple EvoleqFX-Application calculating and showing gauss-distribution for given variance and expectation
* [ ] EvoleqFX-Application: file manager 


