plugins {
    id ("com.github.hierynomus.license") version "0.15.0"
}

group = Config.Project.group
version = Config.Project.version

allprojects {
    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
    }

}