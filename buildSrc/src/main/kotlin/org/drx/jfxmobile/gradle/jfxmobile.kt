package org.drx.jfxmobile.gradle

import org.drx.jfxmobile.gradle.ReleaseExtension
import org.gradle.api.plugins.ExtensionAware
import org.gradle.kotlin.dsl.closureOf
import org.javafxports.jfxmobile.plugin.DownConfiguration
import org.javafxports.jfxmobile.plugin.JFXMobileExtension
import org.javafxports.jfxmobile.plugin.android.AndroidExtension

// Work around https://github.com/gradle/kotlin-dsl/issues/457
fun JFXMobileExtension.android(action: AndroidExtension.() -> Unit) =
        (this as ExtensionAware).extensions.configure(AndroidExtension::class.java, action)

fun JFXMobileExtension.downConfig(action: DownConfiguration.() -> Unit) = downConfig(closureOf ( action ) )

fun AndroidExtension.buildTypes(action: BuildTypesExtension.()->Unit) =
        (this as ExtensionAware).extensions.configure(BuildTypesExtension::class.java, action)

fun BuildTypesExtension.release(action: ReleaseExtension.()->Unit) =
        (this as ExtensionAware).extensions.configure(ReleaseExtension::class.java, action)
