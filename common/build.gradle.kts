


//import org.drx.plugin.algebraictypes.*
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    java
    kotlin("jvm") version Config.Versions.kotlin
}
/*
buildscript{
    repositories {
        mavenLocal()
        mavenCentral()
        jcenter()
    }
    dependencies{
        classpath(Config.Dependencies.algebraicTypes)
    }
}
*/

repositories {
    mavenCentral()
    jcenter()
}
dependencies {
    implementation (Config.Dependencies.kotlinStandardLibrary)
    implementation (Config.Dependencies.coroutines)
    implementation ( Config.Dependencies.kotlinReflect )

    //implementation(Config.Dependencies.tornadofx)

    implementation (Config.Dependencies.evoleq)
    implementation (Config.Dependencies.evoleqfx)


    testCompile("junit", "junit", "4.12")
    //testCompile ("org.testfx:testfx-core:4.0.15-alpha")
    //testCompile ("org.testfx:testfx-junit:4.0.15-alpha")
    //implementation(kotlinModule("stdlib-jdk8", kotlin_version))
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8

    sourceSets.create("generated"){
        java.srcDirs("src/generated/java", "src/generated/kotlin")
    }
    sourceSets.getByName("main"){ java {
        compileClasspath += sourceSets["generated"].output
        runtimeClasspath += sourceSets["generated"].output
    }}
}
sourceSets["generated"].withConvention(KotlinSourceSet::class) {
    kotlin.srcDir("src/generated/kotlin")
    kotlin.srcDir("src/generated/java")
    dependencies{
        implementation ( Config.Dependencies.kotlinStandardLibrary )
    }
}
sourceSets["main"].withConvention(KotlinSourceSet::class) {

}

tasks{
    jar{
        from(sourceSets["generated"].output)
    }
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

/*
apply<KotlinAlgebraicTypesPlugin>()

configure<AlgebraicTypesExtension>() {
    dualities{
        range(2,10)
    }
    productArithmetics{
        dimension(10)
    }
    evoleqProducts{
        range(2,10)
    }
    evoleqSums{
        range(2,10)
    }

    outputs{
        //compile("experiments")
        //runtime("experiments")
    }
}

*/