package org.drx.evoleq.examples.products

import org.drx.evoleq.evolving.Evolving
import org.drx.evoleq.flow.Evolver
import org.drx.generated.products.Product
import org.drx.generated.products.Product2

suspend fun <D2, D1> Product2<Evolver<D2>,Evolver<D1>>.evolve(data: Product2<D2,D1>): Product2<Evolving<D2>,Evolving<D1>> = Product(
        factor2.evolve(data.factor2),
        factor1.evolve(data.factor1)
)

suspend fun <D2, D1> Product2<Evolving<D2>,Evolving<D1>>.get(): Product2<D2,D1> = Product(factor2.get(),factor1.get())