package org.drx.evoleq.examples.products

import org.drx.evoleq.evolving.Evolving
import org.drx.evoleq.flow.Evolver
import org.drx.generated.products.Product
import org.drx.generated.products.Product5

suspend fun <D5, D4, D3, D2, D1> Product5<Evolver<D5>, Evolver<D4>, Evolver<D3>, Evolver<D2>, Evolver<D1>>.evolve(data: Product5<D5, D4, D3, D2, D1>): Product5<Evolving<D5> ,Evolving<D4>, Evolving<D3>, Evolving<D2>, Evolving<D1>> = Product(
        factor5.evolve(data.factor5),
        factor4.evolve(data.factor4),
        factor3.evolve(data.factor3),
        factor2.evolve(data.factor2),
        factor1.evolve(data.factor1)
)

suspend fun <D5, D4, D3, D2, D1> Product5<Evolving<D5> ,Evolving<D4>, Evolving<D3>, Evolving<D2>, Evolving<D1>>.get(): Product5<D5, D4, D3, D2, D1> = Product(factor5.get(), factor4.get(), factor3.get(), factor2.get(),factor1.get())