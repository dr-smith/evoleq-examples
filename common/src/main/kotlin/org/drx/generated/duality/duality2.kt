/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.generated.duality



import org.drx.generated.products.Product2
import org.drx.generated.sums.*



fun <F2, F1 ,T> Product2<(F2)->T, (F1)->T>.oppose(): (Sum2<F2, F1>)->T = sum(
    factor2,
    factor1
)

fun <F2, F1 ,T> ((Sum2<F2, F1>)->T).oppose(): Product2<(F2)->T, (F1)->T> = Product2(
    {f: F2 -> this@oppose(iota2_2<F2, F1>()(f))},
    {f: F1 -> this@oppose(iota2_1<F2, F1>()(f))}
)

/**
 * Measure a sum type
 */
fun <M, T2, T1> Sum2<T2, T1>.measure(measure: Product2<(T2) -> M, (T1) -> M>): M = when( this ) {
    is Sum2.Summand2 -> measure.factor2( value )
    is Sum2.Summand1 -> measure.factor1( value )
}

/**
 * Measure a sum type
 */
fun <M, T> Sum2<T, T>.measure(measure: (T) -> M): M = when( this ) {
    is Sum2.Summand2 -> measure( value )
    is Sum2.Summand1 -> measure( value )
}