/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.generated.duality



import org.drx.generated.products.Product4
import org.drx.generated.sums.*



fun <F4, F3, F2, F1 ,T> Product4<(F4)->T, (F3)->T, (F2)->T, (F1)->T>.oppose(): (Sum4<F4, F3, F2, F1>)->T = sum(
    factor4,
    factor3,
    factor2,
    factor1
)

fun <F4, F3, F2, F1 ,T> ((Sum4<F4, F3, F2, F1>)->T).oppose(): Product4<(F4)->T, (F3)->T, (F2)->T, (F1)->T> = Product4(
    {f: F4 -> this@oppose(iota4_4<F4, F3, F2, F1>()(f))},
    {f: F3 -> this@oppose(iota4_3<F4, F3, F2, F1>()(f))},
    {f: F2 -> this@oppose(iota4_2<F4, F3, F2, F1>()(f))},
    {f: F1 -> this@oppose(iota4_1<F4, F3, F2, F1>()(f))}
)

/**
 * Measure a sum type
 */
fun <M, T4, T3, T2, T1> Sum4<T4, T3, T2, T1>.measure(measure: Product4<(T4) -> M, (T3) -> M, (T2) -> M, (T1) -> M>): M = when( this ) {
    is Sum4.Summand4 -> measure.factor4( value )
    is Sum4.Summand3 -> measure.factor3( value )
    is Sum4.Summand2 -> measure.factor2( value )
    is Sum4.Summand1 -> measure.factor1( value )
}

/**
 * Measure a sum type
 */
fun <M, T> Sum4<T, T, T, T>.measure(measure: (T) -> M): M = when( this ) {
    is Sum4.Summand4 -> measure( value )
    is Sum4.Summand3 -> measure( value )
    is Sum4.Summand2 -> measure( value )
    is Sum4.Summand1 -> measure( value )
}