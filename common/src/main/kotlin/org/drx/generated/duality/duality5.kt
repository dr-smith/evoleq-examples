/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.generated.duality



import org.drx.generated.products.Product5
import org.drx.generated.sums.*



fun <F5, F4, F3, F2, F1 ,T> Product5<(F5)->T, (F4)->T, (F3)->T, (F2)->T, (F1)->T>.oppose(): (Sum5<F5, F4, F3, F2, F1>)->T = sum(
    factor5,
    factor4,
    factor3,
    factor2,
    factor1
)

fun <F5, F4, F3, F2, F1 ,T> ((Sum5<F5, F4, F3, F2, F1>)->T).oppose(): Product5<(F5)->T, (F4)->T, (F3)->T, (F2)->T, (F1)->T> = Product5(
    {f: F5 -> this@oppose(iota5_5<F5, F4, F3, F2, F1>()(f))},
    {f: F4 -> this@oppose(iota5_4<F5, F4, F3, F2, F1>()(f))},
    {f: F3 -> this@oppose(iota5_3<F5, F4, F3, F2, F1>()(f))},
    {f: F2 -> this@oppose(iota5_2<F5, F4, F3, F2, F1>()(f))},
    {f: F1 -> this@oppose(iota5_1<F5, F4, F3, F2, F1>()(f))}
)

/**
 * Measure a sum type
 */
fun <M, T5, T4, T3, T2, T1> Sum5<T5, T4, T3, T2, T1>.measure(measure: Product5<(T5) -> M, (T4) -> M, (T3) -> M, (T2) -> M, (T1) -> M>): M = when( this ) {
    is Sum5.Summand5 -> measure.factor5( value )
    is Sum5.Summand4 -> measure.factor4( value )
    is Sum5.Summand3 -> measure.factor3( value )
    is Sum5.Summand2 -> measure.factor2( value )
    is Sum5.Summand1 -> measure.factor1( value )
}

/**
 * Measure a sum type
 */
fun <M, T> Sum5<T, T, T, T, T>.measure(measure: (T) -> M): M = when( this ) {
    is Sum5.Summand5 -> measure( value )
    is Sum5.Summand4 -> measure( value )
    is Sum5.Summand3 -> measure( value )
    is Sum5.Summand2 -> measure( value )
    is Sum5.Summand1 -> measure( value )
}