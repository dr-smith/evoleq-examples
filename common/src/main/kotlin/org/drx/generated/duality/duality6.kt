/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.generated.duality



import org.drx.generated.products.Product6
import org.drx.generated.sums.*



fun <F6, F5, F4, F3, F2, F1 ,T> Product6<(F6)->T, (F5)->T, (F4)->T, (F3)->T, (F2)->T, (F1)->T>.oppose(): (Sum6<F6, F5, F4, F3, F2, F1>)->T = sum(
    factor6,
    factor5,
    factor4,
    factor3,
    factor2,
    factor1
)

fun <F6, F5, F4, F3, F2, F1 ,T> ((Sum6<F6, F5, F4, F3, F2, F1>)->T).oppose(): Product6<(F6)->T, (F5)->T, (F4)->T, (F3)->T, (F2)->T, (F1)->T> = Product6(
    {f: F6 -> this@oppose(iota6_6<F6, F5, F4, F3, F2, F1>()(f))},
    {f: F5 -> this@oppose(iota6_5<F6, F5, F4, F3, F2, F1>()(f))},
    {f: F4 -> this@oppose(iota6_4<F6, F5, F4, F3, F2, F1>()(f))},
    {f: F3 -> this@oppose(iota6_3<F6, F5, F4, F3, F2, F1>()(f))},
    {f: F2 -> this@oppose(iota6_2<F6, F5, F4, F3, F2, F1>()(f))},
    {f: F1 -> this@oppose(iota6_1<F6, F5, F4, F3, F2, F1>()(f))}
)

/**
 * Measure a sum type
 */
fun <M, T6, T5, T4, T3, T2, T1> Sum6<T6, T5, T4, T3, T2, T1>.measure(measure: Product6<(T6) -> M, (T5) -> M, (T4) -> M, (T3) -> M, (T2) -> M, (T1) -> M>): M = when( this ) {
    is Sum6.Summand6 -> measure.factor6( value )
    is Sum6.Summand5 -> measure.factor5( value )
    is Sum6.Summand4 -> measure.factor4( value )
    is Sum6.Summand3 -> measure.factor3( value )
    is Sum6.Summand2 -> measure.factor2( value )
    is Sum6.Summand1 -> measure.factor1( value )
}

/**
 * Measure a sum type
 */
fun <M, T> Sum6<T, T, T, T, T, T>.measure(measure: (T) -> M): M = when( this ) {
    is Sum6.Summand6 -> measure( value )
    is Sum6.Summand5 -> measure( value )
    is Sum6.Summand4 -> measure( value )
    is Sum6.Summand3 -> measure( value )
    is Sum6.Summand2 -> measure( value )
    is Sum6.Summand1 -> measure( value )
}