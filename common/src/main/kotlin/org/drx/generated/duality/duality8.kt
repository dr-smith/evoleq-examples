/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.generated.duality



import org.drx.generated.products.Product8
import org.drx.generated.sums.*



fun <F8, F7, F6, F5, F4, F3, F2, F1 ,T> Product8<(F8)->T, (F7)->T, (F6)->T, (F5)->T, (F4)->T, (F3)->T, (F2)->T, (F1)->T>.oppose(): (Sum8<F8, F7, F6, F5, F4, F3, F2, F1>)->T = sum(
    factor8,
    factor7,
    factor6,
    factor5,
    factor4,
    factor3,
    factor2,
    factor1
)

fun <F8, F7, F6, F5, F4, F3, F2, F1 ,T> ((Sum8<F8, F7, F6, F5, F4, F3, F2, F1>)->T).oppose(): Product8<(F8)->T, (F7)->T, (F6)->T, (F5)->T, (F4)->T, (F3)->T, (F2)->T, (F1)->T> = Product8(
    {f: F8 -> this@oppose(iota8_8<F8, F7, F6, F5, F4, F3, F2, F1>()(f))},
    {f: F7 -> this@oppose(iota8_7<F8, F7, F6, F5, F4, F3, F2, F1>()(f))},
    {f: F6 -> this@oppose(iota8_6<F8, F7, F6, F5, F4, F3, F2, F1>()(f))},
    {f: F5 -> this@oppose(iota8_5<F8, F7, F6, F5, F4, F3, F2, F1>()(f))},
    {f: F4 -> this@oppose(iota8_4<F8, F7, F6, F5, F4, F3, F2, F1>()(f))},
    {f: F3 -> this@oppose(iota8_3<F8, F7, F6, F5, F4, F3, F2, F1>()(f))},
    {f: F2 -> this@oppose(iota8_2<F8, F7, F6, F5, F4, F3, F2, F1>()(f))},
    {f: F1 -> this@oppose(iota8_1<F8, F7, F6, F5, F4, F3, F2, F1>()(f))}
)

/**
 * Measure a sum type
 */
fun <M, T8, T7, T6, T5, T4, T3, T2, T1> Sum8<T8, T7, T6, T5, T4, T3, T2, T1>.measure(measure: Product8<(T8) -> M, (T7) -> M, (T6) -> M, (T5) -> M, (T4) -> M, (T3) -> M, (T2) -> M, (T1) -> M>): M = when( this ) {
    is Sum8.Summand8 -> measure.factor8( value )
    is Sum8.Summand7 -> measure.factor7( value )
    is Sum8.Summand6 -> measure.factor6( value )
    is Sum8.Summand5 -> measure.factor5( value )
    is Sum8.Summand4 -> measure.factor4( value )
    is Sum8.Summand3 -> measure.factor3( value )
    is Sum8.Summand2 -> measure.factor2( value )
    is Sum8.Summand1 -> measure.factor1( value )
}

/**
 * Measure a sum type
 */
fun <M, T> Sum8<T, T, T, T, T, T, T, T>.measure(measure: (T) -> M): M = when( this ) {
    is Sum8.Summand8 -> measure( value )
    is Sum8.Summand7 -> measure( value )
    is Sum8.Summand6 -> measure( value )
    is Sum8.Summand5 -> measure( value )
    is Sum8.Summand4 -> measure( value )
    is Sum8.Summand3 -> measure( value )
    is Sum8.Summand2 -> measure( value )
    is Sum8.Summand1 -> measure( value )
}