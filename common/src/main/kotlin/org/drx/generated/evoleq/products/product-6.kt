/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.evoleq.products


import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import org.drx.evoleq.dsl.parallel
import org.drx.evoleq.flow.Evolver
import org.drx.evoleq.evolving.Evolving
import org.drx.evoleq.evolving.Parallel
import org.drx.generated.products.Product6



/**
 * Evolve a product type with a product evolver
 */
suspend fun <T6, T5, T4, T3, T2, T1> Product6<Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>.evolve(product: Product6<T6, T5, T4, T3, T2, T1>) : Product6<Evolving<T6>, Evolving<T5>, Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>> = Product6(factor6.evolve( product.factor6 ), factor5.evolve( product.factor5 ), factor4.evolve( product.factor4 ), factor3.evolve( product.factor3 ), factor2.evolve( product.factor2 ), factor1.evolve( product.factor1 ))

/**
 * Obtain product evolver with parallel side-effect
 */
@Suppress("FunctionName")
fun <T6, T5, T4, T3, T2, T1> CoroutineScope.Product6Evolver(evolver: Product6<Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>, sideEffect: suspend CoroutineScope.()->Parallel<Unit>): Product6<Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>> {
    launch { parallel { sideEffect().get() } }
    return evolver
}

/**
 * Evolving product getter function
 */
suspend fun <T6, T5, T4, T3, T2, T1> Product6<Evolving<T6>, Evolving<T5>, Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>>.get() : Product6<T6, T5, T4, T3, T2, T1> = Product6(factor6.get(), factor5.get(), factor4.get(), factor3.get(), factor2.get(), factor1.get())