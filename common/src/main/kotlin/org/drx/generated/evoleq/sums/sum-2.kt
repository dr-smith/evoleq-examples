/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.evoleq.sums


import org.drx.evoleq.flow.Evolver
import org.drx.evoleq.evolving.Evolving
import org.drx.generated.products.Product2
import org.drx.generated.sums.Sum2



/**
 * Evolve a sum type
 */
suspend fun <T2, T1> Product2<Evolver<T2>, Evolver<T1>>.evolve(sum: Sum2<T2, T1>) : Sum2<Evolving<T2>, Evolving<T1>> = when( sum ) { 
    is Sum2.Summand2 -> Sum2.Summand2( factor2.evolve( sum.value ) )
    is Sum2.Summand1 -> Sum2.Summand1( factor1.evolve( sum.value ) ) 
}

/**
 * Evolve with respect to sum-type-flow
 */
suspend fun <T2, T1> Sum2<Evolver<T2>, Evolver<T1>>.evolve(product: Product2<T2, T1>) : Sum2<Evolving<T2>, Evolving<T1>> = when( this ) { 
    is Sum2.Summand2 -> Sum2.Summand2( value.evolve( product.factor2 ) )
    is Sum2.Summand1 -> Sum2.Summand1( value.evolve( product.factor1 ) ) 
}

/**
 * Evolve with respect to sum-type-flow
 */
suspend fun <D> Sum2<Evolver<D>, Evolver<D>>.evolve(data: D): Evolving<D> = when( this ) {
    is Sum2.Summand2 -> value.evolve( data )
    is Sum2.Summand1 -> value.evolve( data )
}

/**
 * Evolve with respect to sum-type-flow with dynamic data
 */
suspend fun <D, T2, T1> Product2<Sum2<Evolver<T2>, Evolver<T1>>, Product2<(D) -> T2, (D) -> T1>>.evolve( data : D): Sum2<Evolving<T2>, Evolving<T1>> = when( factor2 ) {
    is Sum2.Summand2 -> Sum2.Summand2( (factor2 as Sum2.Summand2<Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor2( data ) ) )
    is Sum2.Summand1 -> Sum2.Summand1( (factor2 as Sum2.Summand1<Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor1( data ) ) )
}

/**
 * Setup dynamic Sum2 evolver
 */
@Suppress("FunctionName")
fun <D, T2, T1> Sum2Evolver(evolver: Sum2<Evolver<T2>, Evolver<T1>>, data: ()->Product2<(D) -> T2, (D) -> T1>): Product2<Sum2<Evolver<T2>, Evolver<T1>>, Product2<(D) -> T2, (D) -> T1>> = Product2(evolver, data())

suspend fun <T2, T1> Sum2<Evolving<T2>, Evolving<T1>>.get() : Sum2<T2, T1> = when( this ) { 
    is Sum2.Summand2 -> Sum2.Summand2( value.get() )
    is Sum2.Summand1 -> Sum2.Summand1( value.get() ) 
}