/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.evoleq.sums


import org.drx.evoleq.flow.Evolver
import org.drx.evoleq.evolving.Evolving
import org.drx.generated.products.Product2
import org.drx.generated.products.Product4
import org.drx.generated.sums.Sum4



/**
 * Evolve a sum type
 */
suspend fun <T4, T3, T2, T1> Product4<Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>.evolve(sum: Sum4<T4, T3, T2, T1>) : Sum4<Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>> = when( sum ) { 
    is Sum4.Summand4 -> Sum4.Summand4( factor4.evolve( sum.value ) )
    is Sum4.Summand3 -> Sum4.Summand3( factor3.evolve( sum.value ) )
    is Sum4.Summand2 -> Sum4.Summand2( factor2.evolve( sum.value ) )
    is Sum4.Summand1 -> Sum4.Summand1( factor1.evolve( sum.value ) ) 
}

/**
 * Evolve with respect to sum-type-flow
 */
suspend fun <T4, T3, T2, T1> Sum4<Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>.evolve(product: Product4<T4, T3, T2, T1>) : Sum4<Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>> = when( this ) { 
    is Sum4.Summand4 -> Sum4.Summand4( value.evolve( product.factor4 ) )
    is Sum4.Summand3 -> Sum4.Summand3( value.evolve( product.factor3 ) )
    is Sum4.Summand2 -> Sum4.Summand2( value.evolve( product.factor2 ) )
    is Sum4.Summand1 -> Sum4.Summand1( value.evolve( product.factor1 ) ) 
}

/**
 * Evolve with respect to sum-type-flow
 */
suspend fun <D> Sum4<Evolver<D>, Evolver<D>, Evolver<D>, Evolver<D>>.evolve(data: D): Evolving<D> = when( this ) {
    is Sum4.Summand4 -> value.evolve( data )
    is Sum4.Summand3 -> value.evolve( data )
    is Sum4.Summand2 -> value.evolve( data )
    is Sum4.Summand1 -> value.evolve( data )
}

/**
 * Evolve with respect to sum-type-flow with dynamic data
 */
suspend fun <D, T4, T3, T2, T1> Product2<Sum4<Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>, Product4<(D) -> T4, (D) -> T3, (D) -> T2, (D) -> T1>>.evolve( data : D): Sum4<Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>> = when( factor2 ) {
    is Sum4.Summand4 -> Sum4.Summand4( (factor2 as Sum4.Summand4<Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor4( data ) ) )
    is Sum4.Summand3 -> Sum4.Summand3( (factor2 as Sum4.Summand3<Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor3( data ) ) )
    is Sum4.Summand2 -> Sum4.Summand2( (factor2 as Sum4.Summand2<Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor2( data ) ) )
    is Sum4.Summand1 -> Sum4.Summand1( (factor2 as Sum4.Summand1<Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor1( data ) ) )
}

/**
 * Setup dynamic Sum4 evolver
 */
@Suppress("FunctionName")
fun <D, T4, T3, T2, T1> Sum4Evolver(evolver: Sum4<Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>, data: ()->Product4<(D) -> T4, (D) -> T3, (D) -> T2, (D) -> T1>): Product2<Sum4<Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>, Product4<(D) -> T4, (D) -> T3, (D) -> T2, (D) -> T1>> = Product2(evolver, data())

suspend fun <T4, T3, T2, T1> Sum4<Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>>.get() : Sum4<T4, T3, T2, T1> = when( this ) { 
    is Sum4.Summand4 -> Sum4.Summand4( value.get() )
    is Sum4.Summand3 -> Sum4.Summand3( value.get() )
    is Sum4.Summand2 -> Sum4.Summand2( value.get() )
    is Sum4.Summand1 -> Sum4.Summand1( value.get() ) 
}