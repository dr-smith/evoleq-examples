/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.evoleq.sums


import org.drx.evoleq.flow.Evolver
import org.drx.evoleq.evolving.Evolving
import org.drx.generated.products.Product2
import org.drx.generated.products.Product9
import org.drx.generated.sums.Sum9



/**
 * Evolve a sum type
 */
suspend fun <T9, T8, T7, T6, T5, T4, T3, T2, T1> Product9<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>.evolve(sum: Sum9<T9, T8, T7, T6, T5, T4, T3, T2, T1>) : Sum9<Evolving<T9>, Evolving<T8>, Evolving<T7>, Evolving<T6>, Evolving<T5>, Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>> = when( sum ) { 
    is Sum9.Summand9 -> Sum9.Summand9( factor9.evolve( sum.value ) )
    is Sum9.Summand8 -> Sum9.Summand8( factor8.evolve( sum.value ) )
    is Sum9.Summand7 -> Sum9.Summand7( factor7.evolve( sum.value ) )
    is Sum9.Summand6 -> Sum9.Summand6( factor6.evolve( sum.value ) )
    is Sum9.Summand5 -> Sum9.Summand5( factor5.evolve( sum.value ) )
    is Sum9.Summand4 -> Sum9.Summand4( factor4.evolve( sum.value ) )
    is Sum9.Summand3 -> Sum9.Summand3( factor3.evolve( sum.value ) )
    is Sum9.Summand2 -> Sum9.Summand2( factor2.evolve( sum.value ) )
    is Sum9.Summand1 -> Sum9.Summand1( factor1.evolve( sum.value ) ) 
}

/**
 * Evolve with respect to sum-type-flow
 */
suspend fun <T9, T8, T7, T6, T5, T4, T3, T2, T1> Sum9<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>.evolve(product: Product9<T9, T8, T7, T6, T5, T4, T3, T2, T1>) : Sum9<Evolving<T9>, Evolving<T8>, Evolving<T7>, Evolving<T6>, Evolving<T5>, Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>> = when( this ) { 
    is Sum9.Summand9 -> Sum9.Summand9( value.evolve( product.factor9 ) )
    is Sum9.Summand8 -> Sum9.Summand8( value.evolve( product.factor8 ) )
    is Sum9.Summand7 -> Sum9.Summand7( value.evolve( product.factor7 ) )
    is Sum9.Summand6 -> Sum9.Summand6( value.evolve( product.factor6 ) )
    is Sum9.Summand5 -> Sum9.Summand5( value.evolve( product.factor5 ) )
    is Sum9.Summand4 -> Sum9.Summand4( value.evolve( product.factor4 ) )
    is Sum9.Summand3 -> Sum9.Summand3( value.evolve( product.factor3 ) )
    is Sum9.Summand2 -> Sum9.Summand2( value.evolve( product.factor2 ) )
    is Sum9.Summand1 -> Sum9.Summand1( value.evolve( product.factor1 ) ) 
}

/**
 * Evolve with respect to sum-type-flow
 */
suspend fun <D> Sum9<Evolver<D>, Evolver<D>, Evolver<D>, Evolver<D>, Evolver<D>, Evolver<D>, Evolver<D>, Evolver<D>, Evolver<D>>.evolve(data: D): Evolving<D> = when( this ) {
    is Sum9.Summand9 -> value.evolve( data )
    is Sum9.Summand8 -> value.evolve( data )
    is Sum9.Summand7 -> value.evolve( data )
    is Sum9.Summand6 -> value.evolve( data )
    is Sum9.Summand5 -> value.evolve( data )
    is Sum9.Summand4 -> value.evolve( data )
    is Sum9.Summand3 -> value.evolve( data )
    is Sum9.Summand2 -> value.evolve( data )
    is Sum9.Summand1 -> value.evolve( data )
}

/**
 * Evolve with respect to sum-type-flow with dynamic data
 */
suspend fun <D, T9, T8, T7, T6, T5, T4, T3, T2, T1> Product2<Sum9<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>, Product9<(D) -> T9, (D) -> T8, (D) -> T7, (D) -> T6, (D) -> T5, (D) -> T4, (D) -> T3, (D) -> T2, (D) -> T1>>.evolve( data : D): Sum9<Evolving<T9>, Evolving<T8>, Evolving<T7>, Evolving<T6>, Evolving<T5>, Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>> = when( factor2 ) {
    is Sum9.Summand9 -> Sum9.Summand9( (factor2 as Sum9.Summand9<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor9( data ) ) )
    is Sum9.Summand8 -> Sum9.Summand8( (factor2 as Sum9.Summand8<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor8( data ) ) )
    is Sum9.Summand7 -> Sum9.Summand7( (factor2 as Sum9.Summand7<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor7( data ) ) )
    is Sum9.Summand6 -> Sum9.Summand6( (factor2 as Sum9.Summand6<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor6( data ) ) )
    is Sum9.Summand5 -> Sum9.Summand5( (factor2 as Sum9.Summand5<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor5( data ) ) )
    is Sum9.Summand4 -> Sum9.Summand4( (factor2 as Sum9.Summand4<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor4( data ) ) )
    is Sum9.Summand3 -> Sum9.Summand3( (factor2 as Sum9.Summand3<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor3( data ) ) )
    is Sum9.Summand2 -> Sum9.Summand2( (factor2 as Sum9.Summand2<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor2( data ) ) )
    is Sum9.Summand1 -> Sum9.Summand1( (factor2 as Sum9.Summand1<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>).value.evolve( factor1.factor1( data ) ) )
}

/**
 * Setup dynamic Sum9 evolver
 */
@Suppress("FunctionName")
fun <D, T9, T8, T7, T6, T5, T4, T3, T2, T1> Sum9Evolver(evolver: Sum9<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>, data: ()->Product9<(D) -> T9, (D) -> T8, (D) -> T7, (D) -> T6, (D) -> T5, (D) -> T4, (D) -> T3, (D) -> T2, (D) -> T1>): Product2<Sum9<Evolver<T9>, Evolver<T8>, Evolver<T7>, Evolver<T6>, Evolver<T5>, Evolver<T4>, Evolver<T3>, Evolver<T2>, Evolver<T1>>, Product9<(D) -> T9, (D) -> T8, (D) -> T7, (D) -> T6, (D) -> T5, (D) -> T4, (D) -> T3, (D) -> T2, (D) -> T1>> = Product2(evolver, data())

suspend fun <T9, T8, T7, T6, T5, T4, T3, T2, T1> Sum9<Evolving<T9>, Evolving<T8>, Evolving<T7>, Evolving<T6>, Evolving<T5>, Evolving<T4>, Evolving<T3>, Evolving<T2>, Evolving<T1>>.get() : Sum9<T9, T8, T7, T6, T5, T4, T3, T2, T1> = when( this ) { 
    is Sum9.Summand9 -> Sum9.Summand9( value.get() )
    is Sum9.Summand8 -> Sum9.Summand8( value.get() )
    is Sum9.Summand7 -> Sum9.Summand7( value.get() )
    is Sum9.Summand6 -> Sum9.Summand6( value.get() )
    is Sum9.Summand5 -> Sum9.Summand5( value.get() )
    is Sum9.Summand4 -> Sum9.Summand4( value.get() )
    is Sum9.Summand3 -> Sum9.Summand3( value.get() )
    is Sum9.Summand2 -> Sum9.Summand2( value.get() )
    is Sum9.Summand1 -> Sum9.Summand1( value.get() ) 
}