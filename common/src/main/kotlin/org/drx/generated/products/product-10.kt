/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.products


data class Product10<out F10, out F9, out F8, out F7, out F6, out F5, out F4, out F3, out F2, out F1>(val factor10: F10, val factor9: F9, val factor8: F8, val factor7: F7, val factor6: F6, val factor5: F5, val factor4: F4, val factor3: F3, val factor2: F2, val factor1: F1) : Product


@Suppress("FunctionName")
fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product(factor10: F10, factor9: F9, factor8: F8, factor7: F7, factor6: F6, factor5: F5, factor4: F4, factor3: F3, factor2: F2, factor1: F1) : Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> = Product10(factor10, factor9, factor8, factor7, factor6, factor5, factor4, factor3, factor2, factor1)

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1, T10, T9, T8, T7, T6, T5, T4, T3, T2, T1> product(f10:(S10)->T10, f9:(S9)->T9, f8:(S8)->T8, f7:(S7)->T7, f6:(S6)->T6, f5:(S5)->T5, f4:(S4)->T4, f3:(S3)->T3, f2:(S2)->T2, f1:(S1)->T1): (Product10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>) -> (Product10<T10, T9, T8, T7, T6, T5, T4, T3, T2, T1>) = { product -> Product(f10(product.factor10), f9(product.factor9), f8(product.factor8), f7(product.factor7), f6(product.factor6), f5(product.factor5), f4(product.factor4), f3(product.factor3), f2(product.factor2), f1(product.factor1)) }



fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_1() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F1 = { product -> product.factor1 }

fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_2() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F2 = { product -> product.factor2 }

fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_3() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F3 = { product -> product.factor3 }

fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_4() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F4 = { product -> product.factor4 }

fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_5() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F5 = { product -> product.factor5 }

fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_6() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F6 = { product -> product.factor6 }

fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_7() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F7 = { product -> product.factor7 }

fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_8() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F8 = { product -> product.factor8 }

fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_9() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F9 = { product -> product.factor9 }

fun <F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> pi10_10() : (Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>) -> F10 = { product -> product.factor10 }



infix fun <G1, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map1(f:(F1)->G1) : Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, G1> = Product(factor10, factor9, factor8, factor7, factor6, factor5, factor4, factor3, factor2, f(factor1))

infix fun <G2, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map2(f:(F2)->G2) : Product10<F10, F9, F8, F7, F6, F5, F4, F3, G2, F1> = Product(factor10, factor9, factor8, factor7, factor6, factor5, factor4, factor3, f(factor2), factor1)

infix fun <G3, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map3(f:(F3)->G3) : Product10<F10, F9, F8, F7, F6, F5, F4, G3, F2, F1> = Product(factor10, factor9, factor8, factor7, factor6, factor5, factor4, f(factor3), factor2, factor1)

infix fun <G4, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map4(f:(F4)->G4) : Product10<F10, F9, F8, F7, F6, F5, G4, F3, F2, F1> = Product(factor10, factor9, factor8, factor7, factor6, factor5, f(factor4), factor3, factor2, factor1)

infix fun <G5, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map5(f:(F5)->G5) : Product10<F10, F9, F8, F7, F6, G5, F4, F3, F2, F1> = Product(factor10, factor9, factor8, factor7, factor6, f(factor5), factor4, factor3, factor2, factor1)

infix fun <G6, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map6(f:(F6)->G6) : Product10<F10, F9, F8, F7, G6, F5, F4, F3, F2, F1> = Product(factor10, factor9, factor8, factor7, f(factor6), factor5, factor4, factor3, factor2, factor1)

infix fun <G7, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map7(f:(F7)->G7) : Product10<F10, F9, F8, G7, F6, F5, F4, F3, F2, F1> = Product(factor10, factor9, factor8, f(factor7), factor6, factor5, factor4, factor3, factor2, factor1)

infix fun <G8, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map8(f:(F8)->G8) : Product10<F10, F9, G8, F7, F6, F5, F4, F3, F2, F1> = Product(factor10, factor9, f(factor8), factor7, factor6, factor5, factor4, factor3, factor2, factor1)

infix fun <G9, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map9(f:(F9)->G9) : Product10<F10, G9, F8, F7, F6, F5, F4, F3, F2, F1> = Product(factor10, f(factor9), factor8, factor7, factor6, factor5, factor4, factor3, factor2, factor1)

infix fun <G10, F10, F9, F8, F7, F6, F5, F4, F3, F2, F1> Product10<F10, F9, F8, F7, F6, F5, F4, F3, F2, F1>.map10(f:(F10)->G10) : Product10<G10, F9, F8, F7, F6, F5, F4, F3, F2, F1> = Product(f(factor10), factor9, factor8, factor7, factor6, factor5, factor4, factor3, factor2, factor1)