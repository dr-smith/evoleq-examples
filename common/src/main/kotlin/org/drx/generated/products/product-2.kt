/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.products


data class Product2<out F2, out F1>(val factor2: F2, val factor1: F1) : Product


@Suppress("FunctionName")
fun <F2, F1> Product(factor2: F2, factor1: F1) : Product2<F2, F1> = Product2(factor2, factor1)

fun <S2, S1, T2, T1> product(f2:(S2)->T2, f1:(S1)->T1): (Product2<S2, S1>) -> (Product2<T2, T1>) = { product -> Product(f2(product.factor2), f1(product.factor1)) }



fun <F2, F1> pi2_1() : (Product2<F2, F1>) -> F1 = { product -> product.factor1 }

fun <F2, F1> pi2_2() : (Product2<F2, F1>) -> F2 = { product -> product.factor2 }



infix fun <G1, F2, F1> Product2<F2, F1>.map1(f:(F1)->G1) : Product2<F2, G1> = Product(factor2, f(factor1))

infix fun <G2, F2, F1> Product2<F2, F1>.map2(f:(F2)->G2) : Product2<G2, F1> = Product(f(factor2), factor1)