/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.products


data class Product3<out F3, out F2, out F1>(val factor3: F3, val factor2: F2, val factor1: F1) : Product


@Suppress("FunctionName")
fun <F3, F2, F1> Product(factor3: F3, factor2: F2, factor1: F1) : Product3<F3, F2, F1> = Product3(factor3, factor2, factor1)

fun <S3, S2, S1, T3, T2, T1> product(f3:(S3)->T3, f2:(S2)->T2, f1:(S1)->T1): (Product3<S3, S2, S1>) -> (Product3<T3, T2, T1>) = { product -> Product(f3(product.factor3), f2(product.factor2), f1(product.factor1)) }



fun <F3, F2, F1> pi3_1() : (Product3<F3, F2, F1>) -> F1 = { product -> product.factor1 }

fun <F3, F2, F1> pi3_2() : (Product3<F3, F2, F1>) -> F2 = { product -> product.factor2 }

fun <F3, F2, F1> pi3_3() : (Product3<F3, F2, F1>) -> F3 = { product -> product.factor3 }



infix fun <G1, F3, F2, F1> Product3<F3, F2, F1>.map1(f:(F1)->G1) : Product3<F3, F2, G1> = Product(factor3, factor2, f(factor1))

infix fun <G2, F3, F2, F1> Product3<F3, F2, F1>.map2(f:(F2)->G2) : Product3<F3, G2, F1> = Product(factor3, f(factor2), factor1)

infix fun <G3, F3, F2, F1> Product3<F3, F2, F1>.map3(f:(F3)->G3) : Product3<G3, F2, F1> = Product(f(factor3), factor2, factor1)