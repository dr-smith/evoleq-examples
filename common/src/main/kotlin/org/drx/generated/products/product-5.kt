/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.products


data class Product5<out F5, out F4, out F3, out F2, out F1>(val factor5: F5, val factor4: F4, val factor3: F3, val factor2: F2, val factor1: F1) : Product


@Suppress("FunctionName")
fun <F5, F4, F3, F2, F1> Product(factor5: F5, factor4: F4, factor3: F3, factor2: F2, factor1: F1) : Product5<F5, F4, F3, F2, F1> = Product5(factor5, factor4, factor3, factor2, factor1)

fun <S5, S4, S3, S2, S1, T5, T4, T3, T2, T1> product(f5:(S5)->T5, f4:(S4)->T4, f3:(S3)->T3, f2:(S2)->T2, f1:(S1)->T1): (Product5<S5, S4, S3, S2, S1>) -> (Product5<T5, T4, T3, T2, T1>) = { product -> Product(f5(product.factor5), f4(product.factor4), f3(product.factor3), f2(product.factor2), f1(product.factor1)) }



fun <F5, F4, F3, F2, F1> pi5_1() : (Product5<F5, F4, F3, F2, F1>) -> F1 = { product -> product.factor1 }

fun <F5, F4, F3, F2, F1> pi5_2() : (Product5<F5, F4, F3, F2, F1>) -> F2 = { product -> product.factor2 }

fun <F5, F4, F3, F2, F1> pi5_3() : (Product5<F5, F4, F3, F2, F1>) -> F3 = { product -> product.factor3 }

fun <F5, F4, F3, F2, F1> pi5_4() : (Product5<F5, F4, F3, F2, F1>) -> F4 = { product -> product.factor4 }

fun <F5, F4, F3, F2, F1> pi5_5() : (Product5<F5, F4, F3, F2, F1>) -> F5 = { product -> product.factor5 }



infix fun <G1, F5, F4, F3, F2, F1> Product5<F5, F4, F3, F2, F1>.map1(f:(F1)->G1) : Product5<F5, F4, F3, F2, G1> = Product(factor5, factor4, factor3, factor2, f(factor1))

infix fun <G2, F5, F4, F3, F2, F1> Product5<F5, F4, F3, F2, F1>.map2(f:(F2)->G2) : Product5<F5, F4, F3, G2, F1> = Product(factor5, factor4, factor3, f(factor2), factor1)

infix fun <G3, F5, F4, F3, F2, F1> Product5<F5, F4, F3, F2, F1>.map3(f:(F3)->G3) : Product5<F5, F4, G3, F2, F1> = Product(factor5, factor4, f(factor3), factor2, factor1)

infix fun <G4, F5, F4, F3, F2, F1> Product5<F5, F4, F3, F2, F1>.map4(f:(F4)->G4) : Product5<F5, G4, F3, F2, F1> = Product(factor5, f(factor4), factor3, factor2, factor1)

infix fun <G5, F5, F4, F3, F2, F1> Product5<F5, F4, F3, F2, F1>.map5(f:(F5)->G5) : Product5<G5, F4, F3, F2, F1> = Product(f(factor5), factor4, factor3, factor2, factor1)