/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.products




operator fun <F4, F3, F2, F1> (Product3<F4, F3, F2>).times(other: F1) : Product4<F4, F3, F2, F1> = Product4(
    factor3,
    factor2,
    factor1,
    other
)

operator fun <F4, F3, F2, F1> (Product2<F4, F3>).times(other: Product2<F2, F1>) : Product4<F4, F3, F2, F1> = Product4(
    factor2,
    factor1,
    other.factor2,
    other.factor1
)

operator fun <F4, F3, F2, F1> (F4).times(other: Product3<F3, F2, F1>) : Product4<F4, F3, F2, F1> = Product4(
    this,
    other.factor3,
    other.factor2,
    other.factor1
)