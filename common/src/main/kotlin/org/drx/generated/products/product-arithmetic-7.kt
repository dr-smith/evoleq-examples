/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.products




operator fun <F7, F6, F5, F4, F3, F2, F1> (Product6<F7, F6, F5, F4, F3, F2>).times(other: F1) : Product7<F7, F6, F5, F4, F3, F2, F1> = Product7(
    factor6,
    factor5,
    factor4,
    factor3,
    factor2,
    factor1,
    other
)

operator fun <F7, F6, F5, F4, F3, F2, F1> (Product5<F7, F6, F5, F4, F3>).times(other: Product2<F2, F1>) : Product7<F7, F6, F5, F4, F3, F2, F1> = Product7(
    factor5,
    factor4,
    factor3,
    factor2,
    factor1,
    other.factor2,
    other.factor1
)

operator fun <F7, F6, F5, F4, F3, F2, F1> (Product4<F7, F6, F5, F4>).times(other: Product3<F3, F2, F1>) : Product7<F7, F6, F5, F4, F3, F2, F1> = Product7(
    factor4,
    factor3,
    factor2,
    factor1,
    other.factor3,
    other.factor2,
    other.factor1
)

operator fun <F7, F6, F5, F4, F3, F2, F1> (Product3<F7, F6, F5>).times(other: Product4<F4, F3, F2, F1>) : Product7<F7, F6, F5, F4, F3, F2, F1> = Product7(
    factor3,
    factor2,
    factor1,
    other.factor4,
    other.factor3,
    other.factor2,
    other.factor1
)

operator fun <F7, F6, F5, F4, F3, F2, F1> (Product2<F7, F6>).times(other: Product5<F5, F4, F3, F2, F1>) : Product7<F7, F6, F5, F4, F3, F2, F1> = Product7(
    factor2,
    factor1,
    other.factor5,
    other.factor4,
    other.factor3,
    other.factor2,
    other.factor1
)

operator fun <F7, F6, F5, F4, F3, F2, F1> (F7).times(other: Product6<F6, F5, F4, F3, F2, F1>) : Product7<F7, F6, F5, F4, F3, F2, F1> = Product7(
    this,
    other.factor6,
    other.factor5,
    other.factor4,
    other.factor3,
    other.factor2,
    other.factor1
)