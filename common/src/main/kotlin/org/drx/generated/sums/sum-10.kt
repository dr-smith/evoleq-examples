/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.sums


sealed class Sum10<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1> : Sum {
    data class Summand1<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S1) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
    data class Summand2<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S2) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
    data class Summand3<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S3) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
    data class Summand4<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S4) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
    data class Summand5<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S5) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
    data class Summand6<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S6) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
    data class Summand7<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S7) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
    data class Summand8<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S8) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
    data class Summand9<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S9) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
    data class Summand10<out S10, out S9, out S8, out S7, out S6, out S5, out S4, out S3, out S2, out S1>(val value: S10) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>()
}

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1, T> sum(
    f10: (S10) -> T,
    f9: (S9) -> T,
    f8: (S8) -> T,
    f7: (S7) -> T,
    f6: (S6) -> T,
    f5: (S5) -> T,
    f4: (S4) -> T,
    f3: (S3) -> T,
    f2: (S2) -> T,
    f1: (S1) -> T
): (Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>) -> T = { sum -> 
    when (sum) {
        is Sum10.Summand10 -> f10(sum.value)
        is Sum10.Summand9 -> f9(sum.value)
        is Sum10.Summand8 -> f8(sum.value)
        is Sum10.Summand7 -> f7(sum.value)
        is Sum10.Summand6 -> f6(sum.value)
        is Sum10.Summand5 -> f5(sum.value)
        is Sum10.Summand4 -> f4(sum.value)
        is Sum10.Summand3 -> f3(sum.value)
        is Sum10.Summand2 -> f2(sum.value)
        is Sum10.Summand1 -> f1(sum.value)
    } 
}



fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_1() : (S1) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s1 -> Sum10.Summand1(s1) }

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_2() : (S2) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s2 -> Sum10.Summand2(s2) }

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_3() : (S3) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s3 -> Sum10.Summand3(s3) }

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_4() : (S4) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s4 -> Sum10.Summand4(s4) }

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_5() : (S5) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s5 -> Sum10.Summand5(s5) }

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_6() : (S6) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s6 -> Sum10.Summand6(s6) }

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_7() : (S7) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s7 -> Sum10.Summand7(s7) }

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_8() : (S8) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s8 -> Sum10.Summand8(s8) }

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_9() : (S9) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s9 -> Sum10.Summand9(s9) }

fun <S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> iota10_10() : (S10) -> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = { s10 -> Sum10.Summand10(s10) }



infix fun <T1, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map1(f:(S1)->T1) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, T1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( value )
    is Sum10.Summand9 -> Sum10.Summand9( value )
    is Sum10.Summand8 -> Sum10.Summand8( value )
    is Sum10.Summand7 -> Sum10.Summand7( value )
    is Sum10.Summand6 -> Sum10.Summand6( value )
    is Sum10.Summand5 -> Sum10.Summand5( value )
    is Sum10.Summand4 -> Sum10.Summand4( value )
    is Sum10.Summand3 -> Sum10.Summand3( value )
    is Sum10.Summand2 -> Sum10.Summand2( value )
    is Sum10.Summand1 -> Sum10.Summand1( f( value ) )
}

infix fun <T2, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map2(f:(S2)->T2) : Sum10<S10, S9, S8, S7, S6, S5, S4, S3, T2, S1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( value )
    is Sum10.Summand9 -> Sum10.Summand9( value )
    is Sum10.Summand8 -> Sum10.Summand8( value )
    is Sum10.Summand7 -> Sum10.Summand7( value )
    is Sum10.Summand6 -> Sum10.Summand6( value )
    is Sum10.Summand5 -> Sum10.Summand5( value )
    is Sum10.Summand4 -> Sum10.Summand4( value )
    is Sum10.Summand3 -> Sum10.Summand3( value )
    is Sum10.Summand2 -> Sum10.Summand2( f( value ) )
    is Sum10.Summand1 -> Sum10.Summand1( value )
}

infix fun <T3, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map3(f:(S3)->T3) : Sum10<S10, S9, S8, S7, S6, S5, S4, T3, S2, S1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( value )
    is Sum10.Summand9 -> Sum10.Summand9( value )
    is Sum10.Summand8 -> Sum10.Summand8( value )
    is Sum10.Summand7 -> Sum10.Summand7( value )
    is Sum10.Summand6 -> Sum10.Summand6( value )
    is Sum10.Summand5 -> Sum10.Summand5( value )
    is Sum10.Summand4 -> Sum10.Summand4( value )
    is Sum10.Summand3 -> Sum10.Summand3( f( value ) )
    is Sum10.Summand2 -> Sum10.Summand2( value )
    is Sum10.Summand1 -> Sum10.Summand1( value )
}

infix fun <T4, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map4(f:(S4)->T4) : Sum10<S10, S9, S8, S7, S6, S5, T4, S3, S2, S1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( value )
    is Sum10.Summand9 -> Sum10.Summand9( value )
    is Sum10.Summand8 -> Sum10.Summand8( value )
    is Sum10.Summand7 -> Sum10.Summand7( value )
    is Sum10.Summand6 -> Sum10.Summand6( value )
    is Sum10.Summand5 -> Sum10.Summand5( value )
    is Sum10.Summand4 -> Sum10.Summand4( f( value ) )
    is Sum10.Summand3 -> Sum10.Summand3( value )
    is Sum10.Summand2 -> Sum10.Summand2( value )
    is Sum10.Summand1 -> Sum10.Summand1( value )
}

infix fun <T5, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map5(f:(S5)->T5) : Sum10<S10, S9, S8, S7, S6, T5, S4, S3, S2, S1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( value )
    is Sum10.Summand9 -> Sum10.Summand9( value )
    is Sum10.Summand8 -> Sum10.Summand8( value )
    is Sum10.Summand7 -> Sum10.Summand7( value )
    is Sum10.Summand6 -> Sum10.Summand6( value )
    is Sum10.Summand5 -> Sum10.Summand5( f( value ) )
    is Sum10.Summand4 -> Sum10.Summand4( value )
    is Sum10.Summand3 -> Sum10.Summand3( value )
    is Sum10.Summand2 -> Sum10.Summand2( value )
    is Sum10.Summand1 -> Sum10.Summand1( value )
}

infix fun <T6, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map6(f:(S6)->T6) : Sum10<S10, S9, S8, S7, T6, S5, S4, S3, S2, S1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( value )
    is Sum10.Summand9 -> Sum10.Summand9( value )
    is Sum10.Summand8 -> Sum10.Summand8( value )
    is Sum10.Summand7 -> Sum10.Summand7( value )
    is Sum10.Summand6 -> Sum10.Summand6( f( value ) )
    is Sum10.Summand5 -> Sum10.Summand5( value )
    is Sum10.Summand4 -> Sum10.Summand4( value )
    is Sum10.Summand3 -> Sum10.Summand3( value )
    is Sum10.Summand2 -> Sum10.Summand2( value )
    is Sum10.Summand1 -> Sum10.Summand1( value )
}

infix fun <T7, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map7(f:(S7)->T7) : Sum10<S10, S9, S8, T7, S6, S5, S4, S3, S2, S1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( value )
    is Sum10.Summand9 -> Sum10.Summand9( value )
    is Sum10.Summand8 -> Sum10.Summand8( value )
    is Sum10.Summand7 -> Sum10.Summand7( f( value ) )
    is Sum10.Summand6 -> Sum10.Summand6( value )
    is Sum10.Summand5 -> Sum10.Summand5( value )
    is Sum10.Summand4 -> Sum10.Summand4( value )
    is Sum10.Summand3 -> Sum10.Summand3( value )
    is Sum10.Summand2 -> Sum10.Summand2( value )
    is Sum10.Summand1 -> Sum10.Summand1( value )
}

infix fun <T8, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map8(f:(S8)->T8) : Sum10<S10, S9, T8, S7, S6, S5, S4, S3, S2, S1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( value )
    is Sum10.Summand9 -> Sum10.Summand9( value )
    is Sum10.Summand8 -> Sum10.Summand8( f( value ) )
    is Sum10.Summand7 -> Sum10.Summand7( value )
    is Sum10.Summand6 -> Sum10.Summand6( value )
    is Sum10.Summand5 -> Sum10.Summand5( value )
    is Sum10.Summand4 -> Sum10.Summand4( value )
    is Sum10.Summand3 -> Sum10.Summand3( value )
    is Sum10.Summand2 -> Sum10.Summand2( value )
    is Sum10.Summand1 -> Sum10.Summand1( value )
}

infix fun <T9, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map9(f:(S9)->T9) : Sum10<S10, T9, S8, S7, S6, S5, S4, S3, S2, S1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( value )
    is Sum10.Summand9 -> Sum10.Summand9( f( value ) )
    is Sum10.Summand8 -> Sum10.Summand8( value )
    is Sum10.Summand7 -> Sum10.Summand7( value )
    is Sum10.Summand6 -> Sum10.Summand6( value )
    is Sum10.Summand5 -> Sum10.Summand5( value )
    is Sum10.Summand4 -> Sum10.Summand4( value )
    is Sum10.Summand3 -> Sum10.Summand3( value )
    is Sum10.Summand2 -> Sum10.Summand2( value )
    is Sum10.Summand1 -> Sum10.Summand1( value )
}

infix fun <T10, S10, S9, S8, S7, S6, S5, S4, S3, S2, S1> Sum10<S10, S9, S8, S7, S6, S5, S4, S3, S2, S1>.map10(f:(S10)->T10) : Sum10<T10, S9, S8, S7, S6, S5, S4, S3, S2, S1> = when(this){ 
    is Sum10.Summand10 -> Sum10.Summand10( f( value ) )
    is Sum10.Summand9 -> Sum10.Summand9( value )
    is Sum10.Summand8 -> Sum10.Summand8( value )
    is Sum10.Summand7 -> Sum10.Summand7( value )
    is Sum10.Summand6 -> Sum10.Summand6( value )
    is Sum10.Summand5 -> Sum10.Summand5( value )
    is Sum10.Summand4 -> Sum10.Summand4( value )
    is Sum10.Summand3 -> Sum10.Summand3( value )
    is Sum10.Summand2 -> Sum10.Summand2( value )
    is Sum10.Summand1 -> Sum10.Summand1( value )
}