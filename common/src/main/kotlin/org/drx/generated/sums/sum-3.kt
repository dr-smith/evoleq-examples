/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.sums


sealed class Sum3<out S3, out S2, out S1> : Sum {
    data class Summand1<out S3, out S2, out S1>(val value: S1) : Sum3<S3, S2, S1>()
    data class Summand2<out S3, out S2, out S1>(val value: S2) : Sum3<S3, S2, S1>()
    data class Summand3<out S3, out S2, out S1>(val value: S3) : Sum3<S3, S2, S1>()
}

fun <S3, S2, S1, T> sum(
    f3: (S3) -> T,
    f2: (S2) -> T,
    f1: (S1) -> T
): (Sum3<S3, S2, S1>) -> T = { sum -> 
    when (sum) {
        is Sum3.Summand3 -> f3(sum.value)
        is Sum3.Summand2 -> f2(sum.value)
        is Sum3.Summand1 -> f1(sum.value)
    } 
}



fun <S3, S2, S1> iota3_1() : (S1) -> Sum3<S3, S2, S1> = { s1 -> Sum3.Summand1(s1) }

fun <S3, S2, S1> iota3_2() : (S2) -> Sum3<S3, S2, S1> = { s2 -> Sum3.Summand2(s2) }

fun <S3, S2, S1> iota3_3() : (S3) -> Sum3<S3, S2, S1> = { s3 -> Sum3.Summand3(s3) }



infix fun <T1, S3, S2, S1> Sum3<S3, S2, S1>.map1(f:(S1)->T1) : Sum3<S3, S2, T1> = when(this){ 
    is Sum3.Summand3 -> Sum3.Summand3( value )
    is Sum3.Summand2 -> Sum3.Summand2( value )
    is Sum3.Summand1 -> Sum3.Summand1( f( value ) )
}

infix fun <T2, S3, S2, S1> Sum3<S3, S2, S1>.map2(f:(S2)->T2) : Sum3<S3, T2, S1> = when(this){ 
    is Sum3.Summand3 -> Sum3.Summand3( value )
    is Sum3.Summand2 -> Sum3.Summand2( f( value ) )
    is Sum3.Summand1 -> Sum3.Summand1( value )
}

infix fun <T3, S3, S2, S1> Sum3<S3, S2, S1>.map3(f:(S3)->T3) : Sum3<T3, S2, S1> = when(this){ 
    is Sum3.Summand3 -> Sum3.Summand3( f( value ) )
    is Sum3.Summand2 -> Sum3.Summand2( value )
    is Sum3.Summand1 -> Sum3.Summand1( value )
}