/**
 * Copyright (c) 2018-2019 Dr. Florian Schmidt
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.drx.generated.sums


sealed class Sum6<out S6, out S5, out S4, out S3, out S2, out S1> : Sum {
    data class Summand1<out S6, out S5, out S4, out S3, out S2, out S1>(val value: S1) : Sum6<S6, S5, S4, S3, S2, S1>()
    data class Summand2<out S6, out S5, out S4, out S3, out S2, out S1>(val value: S2) : Sum6<S6, S5, S4, S3, S2, S1>()
    data class Summand3<out S6, out S5, out S4, out S3, out S2, out S1>(val value: S3) : Sum6<S6, S5, S4, S3, S2, S1>()
    data class Summand4<out S6, out S5, out S4, out S3, out S2, out S1>(val value: S4) : Sum6<S6, S5, S4, S3, S2, S1>()
    data class Summand5<out S6, out S5, out S4, out S3, out S2, out S1>(val value: S5) : Sum6<S6, S5, S4, S3, S2, S1>()
    data class Summand6<out S6, out S5, out S4, out S3, out S2, out S1>(val value: S6) : Sum6<S6, S5, S4, S3, S2, S1>()
}

fun <S6, S5, S4, S3, S2, S1, T> sum(
    f6: (S6) -> T,
    f5: (S5) -> T,
    f4: (S4) -> T,
    f3: (S3) -> T,
    f2: (S2) -> T,
    f1: (S1) -> T
): (Sum6<S6, S5, S4, S3, S2, S1>) -> T = { sum -> 
    when (sum) {
        is Sum6.Summand6 -> f6(sum.value)
        is Sum6.Summand5 -> f5(sum.value)
        is Sum6.Summand4 -> f4(sum.value)
        is Sum6.Summand3 -> f3(sum.value)
        is Sum6.Summand2 -> f2(sum.value)
        is Sum6.Summand1 -> f1(sum.value)
    } 
}



fun <S6, S5, S4, S3, S2, S1> iota6_1() : (S1) -> Sum6<S6, S5, S4, S3, S2, S1> = { s1 -> Sum6.Summand1(s1) }

fun <S6, S5, S4, S3, S2, S1> iota6_2() : (S2) -> Sum6<S6, S5, S4, S3, S2, S1> = { s2 -> Sum6.Summand2(s2) }

fun <S6, S5, S4, S3, S2, S1> iota6_3() : (S3) -> Sum6<S6, S5, S4, S3, S2, S1> = { s3 -> Sum6.Summand3(s3) }

fun <S6, S5, S4, S3, S2, S1> iota6_4() : (S4) -> Sum6<S6, S5, S4, S3, S2, S1> = { s4 -> Sum6.Summand4(s4) }

fun <S6, S5, S4, S3, S2, S1> iota6_5() : (S5) -> Sum6<S6, S5, S4, S3, S2, S1> = { s5 -> Sum6.Summand5(s5) }

fun <S6, S5, S4, S3, S2, S1> iota6_6() : (S6) -> Sum6<S6, S5, S4, S3, S2, S1> = { s6 -> Sum6.Summand6(s6) }



infix fun <T1, S6, S5, S4, S3, S2, S1> Sum6<S6, S5, S4, S3, S2, S1>.map1(f:(S1)->T1) : Sum6<S6, S5, S4, S3, S2, T1> = when(this){ 
    is Sum6.Summand6 -> Sum6.Summand6( value )
    is Sum6.Summand5 -> Sum6.Summand5( value )
    is Sum6.Summand4 -> Sum6.Summand4( value )
    is Sum6.Summand3 -> Sum6.Summand3( value )
    is Sum6.Summand2 -> Sum6.Summand2( value )
    is Sum6.Summand1 -> Sum6.Summand1( f( value ) )
}

infix fun <T2, S6, S5, S4, S3, S2, S1> Sum6<S6, S5, S4, S3, S2, S1>.map2(f:(S2)->T2) : Sum6<S6, S5, S4, S3, T2, S1> = when(this){ 
    is Sum6.Summand6 -> Sum6.Summand6( value )
    is Sum6.Summand5 -> Sum6.Summand5( value )
    is Sum6.Summand4 -> Sum6.Summand4( value )
    is Sum6.Summand3 -> Sum6.Summand3( value )
    is Sum6.Summand2 -> Sum6.Summand2( f( value ) )
    is Sum6.Summand1 -> Sum6.Summand1( value )
}

infix fun <T3, S6, S5, S4, S3, S2, S1> Sum6<S6, S5, S4, S3, S2, S1>.map3(f:(S3)->T3) : Sum6<S6, S5, S4, T3, S2, S1> = when(this){ 
    is Sum6.Summand6 -> Sum6.Summand6( value )
    is Sum6.Summand5 -> Sum6.Summand5( value )
    is Sum6.Summand4 -> Sum6.Summand4( value )
    is Sum6.Summand3 -> Sum6.Summand3( f( value ) )
    is Sum6.Summand2 -> Sum6.Summand2( value )
    is Sum6.Summand1 -> Sum6.Summand1( value )
}

infix fun <T4, S6, S5, S4, S3, S2, S1> Sum6<S6, S5, S4, S3, S2, S1>.map4(f:(S4)->T4) : Sum6<S6, S5, T4, S3, S2, S1> = when(this){ 
    is Sum6.Summand6 -> Sum6.Summand6( value )
    is Sum6.Summand5 -> Sum6.Summand5( value )
    is Sum6.Summand4 -> Sum6.Summand4( f( value ) )
    is Sum6.Summand3 -> Sum6.Summand3( value )
    is Sum6.Summand2 -> Sum6.Summand2( value )
    is Sum6.Summand1 -> Sum6.Summand1( value )
}

infix fun <T5, S6, S5, S4, S3, S2, S1> Sum6<S6, S5, S4, S3, S2, S1>.map5(f:(S5)->T5) : Sum6<S6, T5, S4, S3, S2, S1> = when(this){ 
    is Sum6.Summand6 -> Sum6.Summand6( value )
    is Sum6.Summand5 -> Sum6.Summand5( f( value ) )
    is Sum6.Summand4 -> Sum6.Summand4( value )
    is Sum6.Summand3 -> Sum6.Summand3( value )
    is Sum6.Summand2 -> Sum6.Summand2( value )
    is Sum6.Summand1 -> Sum6.Summand1( value )
}

infix fun <T6, S6, S5, S4, S3, S2, S1> Sum6<S6, S5, S4, S3, S2, S1>.map6(f:(S6)->T6) : Sum6<T6, S5, S4, S3, S2, S1> = when(this){ 
    is Sum6.Summand6 -> Sum6.Summand6( f( value ) )
    is Sum6.Summand5 -> Sum6.Summand5( value )
    is Sum6.Summand4 -> Sum6.Summand4( value )
    is Sum6.Summand3 -> Sum6.Summand3( value )
    is Sum6.Summand2 -> Sum6.Summand2( value )
    is Sum6.Summand1 -> Sum6.Summand1( value )
}