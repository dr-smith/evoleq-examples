import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    java
    kotlin("jvm") version Config.Versions.kotlin
    id ("com.github.hierynomus.license")
}

group  = Config.Project.group
version = Config.Project.SubProjects.AwesomeFXViewer.version


repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
}

application {
    mainClassName = "org.drx.evoleq.examples.fontawesomefxviewer.App"
}

dependencies {
    compile( Config.Dependencies.kotlinStandardLibrary )
    compile( Config.Dependencies.coroutines )
    compile( Config.Dependencies.kotlinReflect )
    compile( Config.Dependencies.evoleq )
    compile( Config.Dependencies.evoleqfx )

    compile ( Config.Dependencies.fonawesomefx )

    compile ( project(":common") )
    //compile ( project(":common", "generated") )

    testImplementation("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}
sourceSets["main"].resources.srcDir("src/main/resources")
sourceSets["main"].withConvention(KotlinSourceSet::class) {
    kotlin.srcDir("src/main/kotlin")
    //resources.srcDir("src/main/resources")
}

val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

license{
    exclude(".css")
}
