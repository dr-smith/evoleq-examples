/**
 * Copyright (c) 2019 Dr. Florian Schmidt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.evoleq.examples.fontawesomefxviewer

import de.jensd.fx.glyphs.fontawesome.FontAwesomeIcon
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon
import de.jensd.fx.glyphs.materialicons.MaterialIcon
import de.jensd.fx.glyphs.octicons.OctIcon
import de.jensd.fx.glyphs.weathericons.WeatherIcon
import javafx.application.Application
import javafx.stage.Stage
import org.drx.evoleq.dsl.parallel
import org.drx.evoleq.examples.fontawesomefxviewer.component.stage.MainStage
import org.drx.evoleq.examples.fontawesomefxviewer.component.stage.mainStage
import org.drx.evoleq.examples.fontawesomefxviewer.data.Data
import org.drx.evoleq.examples.fontawesomefxviewer.data.TabData
import org.drx.evoleq.fx.application.stub.AppManager
import org.drx.evoleq.fx.application.stub.AppMessage
import org.drx.evoleq.fx.component.FxComponent
import org.drx.evoleq.stub.ID
import org.drx.evoleq.stub.Stub

class App : AppManager<Data>() {
    override fun initData(): Data = Data(
            TabData(scope.parallel { arrayListOf( *FontAwesomeIcon.values() ) }),
            TabData(scope.parallel { arrayListOf( *MaterialDesignIcon.values() ) }),
            TabData(scope.parallel { arrayListOf( *MaterialIcon.values() ) }),
            TabData(scope.parallel { arrayListOf( *OctIcon.values() ) }),
            TabData(scope.parallel { arrayListOf( *WeatherIcon.values() ) })
    )

    override suspend fun onDriveStub(stub: Stub<Data>, initialData: Data): AppMessage<Data> = when(stub.id) {
        MainStage::class -> {
            val result = stub.evolve(initialData).get()
            AppMessage.Request.HideStage(MainStage::class, result)
        }
        else ->AppMessage.Process.Terminate(initialData)
    }

    override suspend fun onError(error: AppMessage.Process.Error<Data>): AppMessage<Data> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override suspend fun onStageHidden(id: ID, data: AppMessage<Data>): AppMessage<Data> = when(id) {
        MainStage::class -> AppMessage.Process.Terminate(data.data)
        else  -> AppMessage.Process.Terminate(data.data)
    }

    override suspend fun onStageShown(id: ID, data: AppMessage.Response.StageShown<Data>): AppMessage<Data> = when(id) {
        MainStage::class -> AppMessage.Process.DriveStub(data.stub, data.data)
        else -> throw Exception()
    }

    override suspend fun onStagesRegistered(data: AppMessage<Data>): AppMessage<Data> {
        return AppMessage.Request.ShowStage(MainStage::class, data.data)
    }

    override fun stages(): ArrayList<Pair<ID, () -> FxComponent<Stage, Data>>> {
        return arrayListOf(
                Pair(MainStage::class, mainStage())
        )
    }

}

fun main() {
    Application.launch(App::class.java)
}