/**
 * Copyright (c) 2019 Dr. Florian Schmidt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.evoleq.examples.fontawesomefxviewer.component.stage

import javafx.scene.Scene
import javafx.stage.Screen
import javafx.stage.Stage
import kotlinx.coroutines.delay
import org.drx.evoleq.dsl.parallel
import org.drx.evoleq.dsl.stub
import org.drx.evoleq.examples.fontawesomefxviewer.component.tab.fxIconsTab
import org.drx.evoleq.examples.fontawesomefxviewer.data.Data
import org.drx.evoleq.examples.fontawesomefxviewer.data.TabData
import org.drx.evoleq.examples.fontawesomefxviewer.data.toData
import org.drx.evoleq.examples.fontawesomefxviewer.process.tab.IconTabStubApi
import org.drx.evoleq.examples.fontawesomefxviewer.process.tab.flow
import org.drx.evoleq.examples.products.evolve
import org.drx.evoleq.examples.products.get
import org.drx.evoleq.flow.Process
import org.drx.evoleq.fx.component.FxComponent
import org.drx.evoleq.fx.dsl.*
import org.drx.evoleq.stub.Stub
import org.drx.generated.evoleq.products.Product5Evolver
import org.drx.generated.products.*

class MainStage
class FontAwesomeIcons
class MaterialDesignIcons
class MaterialIcons
class Octicons
class WeatherIcons
fun mainStage(): ()->FxComponent<Stage, Data> = { fxStage{
    id<MainStage>()
    var closeWindowClicked = false
    view{configure{
        title = "FontAwesomeFX Icon Viewer"
        onCloseRequest{
            closeWindowClicked = true
            consume()
        }
    }}
    scene(fxScene{
        tunnel()
        view{configure{}}
        root(fxTabPane {
            tunnel()
            view{configure{ }}
            tabs {
                tab(fxIconsTab("Fontawesome", FontAwesomeIcons::class))
                tab(fxIconsTab("Material Design", MaterialDesignIcons::class))
                tab(fxIconsTab("Material", MaterialIcons::class))
                tab(fxIconsTab("Octicon", Octicons::class))
                tab(fxIconsTab("Weather", WeatherIcons::class))
            }
        }){ root -> Scene(root, Screen.getPrimary().visualBounds.width, Screen.getPrimary().visualBounds.height) }
    })

    lateinit var fontAwesomeStub: Stub<Process.Phase<TabData>>
    lateinit var materialDesignIconsStub: Stub<Process.Phase<TabData>>
    lateinit var materialIconsStub: Stub<Process.Phase<TabData>>
    lateinit var octiconsStub: Stub<Process.Phase<TabData>>
    lateinit var weatherIconsStub: Stub<Process.Phase<TabData>>

    @Suppress("unchecked_cast")
    stubAction {
        fontAwesomeStub = (stubs[FontAwesomeIcons::class]!! as Stub<Process.Phase<TabData>>)
        materialDesignIconsStub = (stubs[MaterialDesignIcons::class]!! as Stub<Process.Phase<TabData>>)
        materialIconsStub = (stubs[MaterialIcons::class]!! as Stub<Process.Phase<TabData>>)
        octiconsStub = (stubs[Octicons::class]!! as Stub<Process.Phase<TabData>>)
        weatherIconsStub = (stubs[WeatherIcons::class]!! as Stub<Process.Phase<TabData>>)
    }

    stub(stub{
        evolve{
            scope.parallel{
                Product5Evolver(Product(
                        fontAwesomeStub.flow(),
                        materialDesignIconsStub.flow(),
                        materialIconsStub.flow(),
                        octiconsStub.flow(),
                        weatherIconsStub.flow()
                )) {
                    // sideEffect:
                    // wait in the background until window closes and
                    // then force the tabs to stop
                    parallel {
                        // wait
                        while (!closeWindowClicked) {
                            delay(1)
                        }
                        // stop tabs
                        Product5(
                                (fontAwesomeStub.stubs[IconTabStubApi::class]!! as Stub<Unit>),
                                (materialDesignIconsStub.stubs[IconTabStubApi::class]!! as Stub<Unit>),
                                (materialIconsStub.stubs[IconTabStubApi::class]!! as Stub<Unit>),
                                (octiconsStub.stubs[IconTabStubApi::class]!! as Stub<Unit>),
                                (weatherIconsStub.stubs[IconTabStubApi::class]!! as Stub<Unit>)
                        ).evolve(Product5(Unit, Unit, Unit, Unit, Unit)).get()
                        Unit
                    }
                }.evolve( Product5(
                        Process.Phase.StartUp.Start(it.fontAwesomeTab),
                        Process.Phase.StartUp.Start(it.materialDesignTab),
                        Process.Phase.StartUp.Start(it.materialTab),
                        Process.Phase.StartUp.Start(it.octiconsTab),
                        Process.Phase.StartUp.Start(it.weatherTab)
                )).get().toData()
            }
        }
    })
}}