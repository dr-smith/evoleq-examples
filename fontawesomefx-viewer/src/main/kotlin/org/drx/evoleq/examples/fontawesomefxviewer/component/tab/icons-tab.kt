/**
 * Copyright (c) 2019 Dr. Florian Schmidt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.evoleq.examples.fontawesomefxviewer.component.tab

import de.jensd.fx.glyphs.GlyphIcons
import de.jensd.fx.glyphs.GlyphsDude
import javafx.beans.property.SimpleStringProperty
import javafx.beans.property.StringProperty
import javafx.collections.ObservableList
import javafx.geometry.Pos
import javafx.scene.Cursor
import javafx.scene.Node
import javafx.scene.control.Label
import javafx.scene.control.Tab
import javafx.scene.control.TabPane
import javafx.scene.layout.VBox
import javafx.scene.text.TextAlignment
import javafx.stage.Screen
import kotlinx.coroutines.delay
import org.drx.evoleq.dsl.parallel
import org.drx.evoleq.evolving.Parallel
import org.drx.evoleq.examples.fontawesomefxviewer.data.Data
import org.drx.evoleq.examples.fontawesomefxviewer.data.TabData
import org.drx.evoleq.examples.fontawesomefxviewer.parallel
import org.drx.evoleq.examples.fontawesomefxviewer.process.tab.IconTabStub
import org.drx.evoleq.examples.fontawesomefxviewer.process.tab.Input
import org.drx.evoleq.flow.Process
import org.drx.evoleq.fx.component.FxComponent
import org.drx.evoleq.fx.dsl.*
import org.drx.evoleq.time.Later
import org.drx.evoleq.time.await
import java.lang.Thread.sleep

fun FxComponentConfiguration<TabPane, Data>.fxIconsTab(tabText: String, stubId: ID) : FxComponent<Tab, Process.Phase<TabData>> = fxTab{
    tunnel()
    view{configure{
        text = tabText
        isClosable = false
    }}
    properties{
        "filterText" to SimpleStringProperty()
    }
    content( fxVBox<Data>{
        tunnel()
        view{configure{}}
        children{
            // menubar
            child(fxAnchorPane<Data>{
                tunnel()
                view{configure{}}
                children{
                    child(fxHBox<Data> {
                        tunnel()
                        view{configure{
                            minHeight = 30.0
                            spacing = 10.0
                            topAnchor(10)
                            leftAnchor(10)
                        }}
                        children{
                            // filter by icon-name
                            child(fxLabel<Nothing>{
                                tunnel()
                                view{configure{
                                    text = "Filter"
                                    prefHeight = 28.0
                                }}
                            })
                            child(fxTextField<Data> {
                                tunnel()
                                view{configure {
                                    prefHeight = 28.0
                                    minWidth = 200.0
                                    this@fxTab.property<StringProperty>("filterText").bind(textProperty())

                                }}

                                tooltip(fxTooltip {
                                    tunnel()//noStub()
                                    view{configure{
                                        text = "Only show icons whose names contain the entered string"
                                    }}
                                })
                            })
                        }
                    })
                }
            })

            // content
            child( fxScrollPane<Process.Phase<TabData>> {
                tunnel()
                view { configure { } }
                content(fxFlowPane<Process.Phase<TabData>> {
                    id(stubId)
                    view {
                        configure {
                            translateX = 10.0
                            hgap = 10.0
                            vgap = 10.0
                            prefWidth = Screen.getPrimary().visualBounds.width - 30.0
                            prefHeight = Screen.getPrimary().visualBounds.height
                        }
                    }
                    // remove when icons appear
                    child(fxLabel<Nothing>{
                        noStub()
                        view{configure{
                            text = "Loading icons ..."
                            translateX = 10.0
                            translateY = 10.0
                        }}
                    })


                    val later: Later<ObservableList<Node>> = Later()
                    val stub = IconTabStub(stubId, later.await() )
                    stub( stub )
                    fxRunTime{
                        later.value = children
                    }
                    this@fxTab.property<StringProperty>("filterText").addListener{_,oldValue,newValue ->
                        if(oldValue != null) {
                            Parallel { stub.input(Input.Data(newValue)) }
                        }
                    }
                })
            })
        }
    })
}

fun glyphBox(icon: GlyphIcons): VBox  {
    val box = VBox()
    box.children.addAll(
            GlyphsDude.createIcon(icon, "100"),
            Label(icon.name())
    )

    box.alignment = Pos.BASELINE_CENTER
    box.spacing = 10.0
    box.autosize()
    return box
}