/**
 * Copyright (c) 2019 Dr. Florian Schmidt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.evoleq.examples.fontawesomefxviewer.data

import de.jensd.fx.glyphs.GlyphIcons
import org.drx.evoleq.evolving.Evolving
import org.drx.evoleq.evolving.Parallel
import org.drx.evoleq.flow.Process
import org.drx.generated.products.*

data class Data(
       val fontAwesomeTab: TabData,
       val materialDesignTab: TabData,
       val materialTab: TabData,
       val octiconsTab: TabData,
       val weatherTab: TabData
)
data class TabData(
        val icons: Evolving<ArrayList<out GlyphIcons>> = Parallel{arrayListOf<GlyphIcons>()},
        val filterText: String = ""
)

fun Product5<Process.Phase<TabData>, Process.Phase<TabData>, Process.Phase<TabData>, Process.Phase<TabData>, Process.Phase<TabData>>.toData(): Data {
    val product5  = map5 { phase -> phase.data }
        .map4 { phase -> phase.data }
        .map3 { phase -> phase.data }
        .map2 { phase -> phase.data }
        .map1 { phase -> phase.data }
    return Data(
            product5.factor5,
            product5.factor4,
            product5.factor3,
            product5.factor2,
            product5.factor1
    )
}

