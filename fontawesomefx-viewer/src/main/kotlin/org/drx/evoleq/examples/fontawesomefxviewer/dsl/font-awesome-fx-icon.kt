package org.drx.evoleq.examples.fontawesomefxviewer.dsl

import de.jensd.fx.glyphs.GlyphIcon
import de.jensd.fx.glyphs.GlyphIcons
import javafx.scene.text.Text
import kotlinx.coroutines.CoroutineScope
import org.drx.evoleq.fx.component.FxComponent
import org.drx.evoleq.fx.dsl.FxComponentConfiguration
import org.drx.evoleq.fx.dsl.fxComponent

class GlyphText(icon: GlyphIcons,size: String = GlyphIcon.DEFAULT_FONT_SIZE) : Text(icon.characterToString()) {
    init{
        styleClass.add("glyph-icon");
        style = String.format("-fx-font-family: %s; -fx-font-size: %s;", icon.fontFamily, size);
    }
}

fun <D> FxComponentConfiguration<out Any, *>.fxGlyphText(scope: CoroutineScope = this.scope, configuration: FxComponentConfiguration<GlyphText, D>.()->Unit): FxComponent<GlyphText, D> = fxComponent(scope,configuration)