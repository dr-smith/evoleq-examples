package org.drx.evoleq.examples.fontawesomefxviewer.process.tab

import javafx.collections.ObservableList
import javafx.scene.Node
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.Job
import org.drx.evoleq.coroutines.BaseReceiver
import org.drx.evoleq.coroutines.Receiver
import org.drx.evoleq.coroutines.onNext
import org.drx.evoleq.dsl.*
import org.drx.evoleq.evolving.Evolving
import org.drx.evoleq.evolving.Parallel
import org.drx.evoleq.examples.fontawesomefxviewer.component.tab.glyphBox
import org.drx.evoleq.examples.fontawesomefxviewer.data.TabData
import org.drx.evoleq.flow.Process
import org.drx.evoleq.fx.dsl.parallelFx
import org.drx.evoleq.stub.ID
import org.drx.evoleq.stub.Stub
import org.drx.evoleq.stub.toFlow
import kotlin.reflect.KClass


sealed class Input {
    data class Init(val data: TabData) : Input()
    data class Data(val filterText: String) : Input()
    object Stop : Input()
}

class IconTabStubApi

class IconTabStub(private val stubId: ID, private val evolvingChildren: Evolving<ObservableList<Node>>) : Stub<Process.Phase<TabData>> {
    override val id: KClass<*>
        get() = stubId
    override val scope: CoroutineScope = CoroutineScope(Job())//DefaultStubScope()

    override val stubs: HashMap<KClass<*>, Stub<*>> by lazy{ hashMapOf<KClass<*>, Stub<*>>()}

    override suspend fun evolve(d: Process.Phase<TabData>): Evolving<Process.Phase<TabData>> = when(val message = d){

        is Process.Phase.StartUp -> when (message) {
            is Process.Phase.StartUp.Start -> scope.parallel {
                children = evolvingChildren.get()
                input(Input.Init(message.data))
                Process.Phase.Runtime.Wait(message.data)
            }
            is Process.Phase.StartUp.Stop ->scope.parallel{
                Process.Phase.Termination.Stop( message.data )
            }
        }
        is Process.Phase.Runtime -> when (message) {
            is Process.Phase.Runtime.Wait ->

                inputStack.onNext<Input,Evolving<Process.Phase<TabData>>> { inputData ->
                    when(inputData) {
                        is Input.Init -> scope.parallel {
                            // init glyphTexts
                            val icons = message.data.icons.get()
                            // add them to children and registry
                            parallelFx {
                                children.clear()
                                children.addAll(icons.map {
                                    val box = glyphBox(it)
                                    registry[it.name()] = box
                                    box
                                })
                                println("added children")
                            }.get()
                            message
                        }
                        is Input.Data -> scope.parallel {
                            val icons = message.data.icons.get()
                            parallelFx {
                                // update gui (filter-mechanism)
                                children.clear()
                                children.addAll( icons
                                        .filter { it.name().toLowerCase().contains(inputData.filterText.toLowerCase()) }
                                        .map { registry[it.name()]!! }
                                )
                            }.get()
                            Process.Phase.Runtime.Wait(message.data.copy(filterText = inputData.filterText))
                        }
                        is Input.Stop -> scope.parallel{
                            println("Received Stop event in $id")
                            Process.Phase.Termination.Stop(message.data)
                        }
                    }
            }
            is Process.Phase.Runtime.Stop -> scope.parallel{
                Process.Phase.Termination.Stop( message.data )
            }
        }
        is Process.Phase.Termination -> when (message) {
            is Process.Phase.Termination.Stop -> scope.parallel {
                Process.Phase.Termination.Dead(message.data)
            }
            is Process.Phase.Termination.Dead -> scope.parallel { message }
        }
    }

    lateinit var children: ObservableList<Node>
    private val registry = hashMapOf<String, Node>()


    /**
     * Runtime API
     */


    private val  input: BaseReceiver<Input> = CoroutineScope(Job()).receiver<Input> {}
            .onNext(CoroutineScope(Job())){input : Input -> inputStack.add(input)}
    private val inputStack: ArrayList<Input> = arrayListOf()
    suspend fun input(input: Input){
        this@IconTabStub.input.send(input)
    }

    init{

        stubs[IconTabStubApi::class] = stub<Unit>{
            id(IconTabStubApi::class)
            evolve{ scope.parallel{
                println("Stopping ${this@IconTabStub.id}")
                input(Input.Stop)}
            }
        }
    }

}

fun Stub<Process.Phase<TabData>>.flow() = this.toFlow<Process.Phase<TabData>,Boolean>(conditions{
    testObject(true)
    check{ b -> b }
    updateCondition { phase: Process.Phase<TabData> -> phase !is Process.Phase.Termination.Dead }
})