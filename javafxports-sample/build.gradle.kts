import org.drx.jfxmobile.gradle.*
import org.jetbrains.kotlin.gradle.plugin.KotlinSourceSet
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    java
    kotlin("jvm") version Config.Versions.kotlin
    id("org.javafxports.jfxmobile")
    id("me.tatarka.retrolambda") version "3.7.1"
    id ("com.github.hierynomus.license")
}

group  = Config.Project.group
version = Config.Project.SubProjects.JavaFXPortsSample.version


repositories {
    mavenLocal()
    mavenCentral()
    jcenter()
    maven( Config.Repositories.googleMvn )
    maven( Config.Repositories.jitpackIo )
}

application {
    mainClassName = "org.drx.evoleq.examples.javafxports.App"
}

dependencies {
    compile( Config.Dependencies.kotlinStandardLibrary )
    compile( Config.Dependencies.coroutines )
    compile( Config.Dependencies.kotlinReflect )
    compile( Config.Dependencies.evoleq )
    compile( Config.Dependencies.evoleqfx )

    testImplementation("junit", "junit", "4.12")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}

sourceSets["android"].withConvention(KotlinSourceSet::class) {
    kotlin.srcDir("src/android/kotlin")
    kotlin.srcDir("src/android/java")
}
sourceSets["desktop"].withConvention(KotlinSourceSet::class) {
    kotlin.srcDir("src/desktop/kotlin")
    /*
    dependencies{
        implementation(Config.Dependencies.evoleq)
        implementation(Config.Dependencies.evoleqfx)
    }
     */
}



repositories {
    jcenter()
    /*
    maven {
        url = uri("http://nexus.gluonhq.com/nexus/content/repositories/releases")
    }
     */
}
jfxmobile {
    /*
        downConfig {
            //version = "3.8.0"
            plugins ("display", "lifecycle", "statusbar", "storage")
        }
     */
    javafxportsVersion = Config.Versions.javafxports
    android {
        manifest = "src/android/AndroidManifest.xml"
        applicationPackage = "org.drx.evoleq.examples.javafxports"
        minSdkVersion = "19"
        //proguardFile = "src/android/proguard-rules.pro"

        dexOptions {
            javaMaxHeapSize =  "3g"

            additionalParameters(
                    "--min-sdk-version=19",
                    "--multi-dex",
                    "--minimal-main-dex",
                    "--force-jumbo"
            )
        }
        packagingOptions {
            exclude ("META-INF/LICENSE.txt")
            exclude ("META-INF/NOTICE.txt")
        }
    }
}



val compileKotlin: KotlinCompile by tasks
compileKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileAndroidKotlin: KotlinCompile by tasks
compileAndroidKotlin.kotlinOptions {
    jvmTarget = "1.8"
}

val compileTestKotlin: KotlinCompile by tasks
compileTestKotlin.kotlinOptions {
    jvmTarget = "1.8"
}
