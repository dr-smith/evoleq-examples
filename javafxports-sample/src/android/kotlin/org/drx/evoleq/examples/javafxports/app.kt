/**
 * Copyright (c) 2019 Dr. Florian Schmidt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.evoleq.examples.javafxports

import javafx.application.Application
import javafx.stage.Stage
import org.drx.evoleq.examples.android.app.component.stage.MainStage
import org.drx.evoleq.examples.android.app.component.stage.mainStage
import org.drx.evoleq.examples.javafxports.component.stage.ErrorStage

import org.drx.evoleq.fx.application.stub.AppMessage
import org.drx.evoleq.fx.application.stub.AppManager
import org.drx.evoleq.fx.component.FxComponent
import org.drx.evoleq.stub.ID
import org.drx.evoleq.stub.Stub


class App : AppManager<Unit>() {

    override fun initData() = Unit

    override fun stages(): ArrayList<Pair<ID, () -> FxComponent<Stage, Unit>>> = arrayListOf(
        Pair( MainStage::class, mainStage() )
    )

    override suspend fun onStageShown(id: ID, data: AppMessage.Response.StageShown<Unit>): AppMessage<Unit> {
        println("onStageShown - id = $id")
        return AppMessage.Process.DriveStub(data.stub, Unit)
    }

    override suspend fun onStageHidden(id: ID, data: AppMessage<Unit>): AppMessage<Unit> = when(id) {
        MainStage::class -> AppMessage.Process.Terminate<Unit>(data.data)
        else -> AppMessage.Process.Terminate<Unit>(data.data)
    }

    override suspend fun onStagesRegistered(data: AppMessage<Unit>): AppMessage<Unit> {
        return AppMessage.Request.ShowStage(MainStage::class, data.data)
    }

    override suspend fun onDriveStub(stub: Stub<Unit>, initialData: Unit): AppMessage<Unit> = when(stub.id){
        MainStage::class -> {
            stub.evolve(initialData).get()
            AppMessage.Request.HideStage(MainStage::class,initialData)
        }
        else -> throw Exception("Pipikind")
    }

    /**
     * todo think about right process
     */
    override suspend fun onError(error: AppMessage.Process.Error<Unit>): AppMessage<Unit> {
        return AppMessage.Request.ShowStage(ErrorStage::class,error.data)
    }
}

fun main() {
    Application.launch(App::class.java)
}

