/**
 * Copyright (c) 2019 Dr. Florian Schmidt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.evoleq.examples.javafxports.component.stage

import javafx.scene.Scene
import javafx.stage.Screen
import javafx.stage.Stage
import kotlinx.coroutines.delay
import org.drx.evoleq.dsl.parallel
import org.drx.evoleq.dsl.stub
import org.drx.evoleq.evolving.Parallel
import org.drx.evoleq.fx.application.stub.AppMessage
import org.drx.evoleq.fx.component.FxComponent
import org.drx.evoleq.fx.dsl.*
import org.drx.evoleq.stub.Stub

class ErrorStage
class OkButton
fun <Data> errorStage(): ()-> FxComponent<Stage, AppMessage.Process.Error<Data>> = {fxStage stub@{

    var okButtonClicked = false

    id<ErrorStage>()
    view{configure{
        title = "Error"
        onCloseRequest{
            if(!okButtonClicked){
                consume()
            }
            okButtonClicked = true
        }
    }}

    scene(fxScene {
        val visualBounds = Screen.getPrimary().visualBounds
        val width = visualBounds.width
        val height = visualBounds.height
        tunnel()
        view{configure{

        }}
        root(fxAnchorPane{
            tunnel()
            view{configure{

            }}
            children{
                child(fxHBox<Unit> {
                    tunnel()
                    view{configure{
                        bottomAnchor(10)
                        rightAnchor(10)
                    }}
                    child {
                        fxButton{
                            id<OkButton>()
                            view{configure{
                                text = "Ok"
                                // = Color.CRIMSON
                            }}
                            stub(stub{
                                evolve {
                                    while (!okButtonClicked) {
                                        delay(1)
                                    }
                                    parallel{}
                                }
                            })
                        }
                    }
                })
            }
        }){
                root -> Scene(root, width, height)
        }
    })

    stub(stub{
        evolve{
            Parallel {
                val buttonStub = stubs[OkButton::class]!! as Stub<Unit>
                buttonStub.evolve(Unit).get()
                it
                //AppMessage.Request.HideStage<Data>(ErrorStage::class)
            }
        }
    })
} }