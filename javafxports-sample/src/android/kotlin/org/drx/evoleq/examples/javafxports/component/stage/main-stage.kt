/**
 * Copyright (c) 2019 Dr. Florian Schmidt
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.drx.evoleq.examples.android.app.component.stage

import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.effect.Reflection
import javafx.scene.layout.Pane
import javafx.scene.text.Font
import javafx.scene.text.FontWeight
import javafx.stage.Screen
import javafx.stage.Stage
import kotlinx.coroutines.delay
import org.drx.evoleq.evolving.Parallel
import org.drx.evoleq.examples.javafxports.App
import org.drx.evoleq.examples.javafxports.util.resource
import org.drx.evoleq.fx.component.FxComponent
import org.drx.evoleq.fx.dsl.*

class MainStage
fun mainStage(): ()-> FxComponent<Stage, Unit> = { fxStage/*(CoroutineScope(Job()))*/{

    var closeStage = false

    id<MainStage>()
    view{configure{

        onCloseRequest{
            if(!closeStage) {
                consume()
            }
            closeStage = true
        }
    }}
    scene(fxScene scene@{
        id<Scene>()
        stylesheet(App::class.resource("css/style.css"))
        view{configure{}}
        properties{
            val visualBounds = Screen.getPrimary().visualBounds
            "width" to visualBounds.width
            "height" to visualBounds.height
        }


        root(fxAnchorPane{
            id<Pane>()
            view{configure{}}
            children{
                child(fxStackPane<Nothing> {
                    noStub()
                    view{configure{
                        topAnchor(10)
                        leftAnchor(10)
                    }}
                    child(fxText<Nothing> {
                        noStub()
                        view{configure{
                            text = "EvoleqFX"
                            font = Font.font(100.0)
                            effect = Reflection()
                        }}
                        fxRunTime{
                            scaleX = (scene.width - 20.0) / boundsInLocal.width
                            scaleY = scaleX
                            translateX = -boundsInParent.minX
                            translateY = -boundsInParent.minY
                        }
                    })
                })
                child(fxButton{
                    id<Button>()
                    view { configure{
                        text = "Close Application"
                        bottomAnchor(10)
                        rightAnchor(10)

                        cssClass("close-button")

                        action{

                            println("button clicked")
                            if(!closeStage) {
                                consume()
                            }
                            closeStage = true
                        }
                    } }
                    stub(org.drx.evoleq.dsl.stub{

                    })
                })
            }


            stub(org.drx.evoleq.dsl.stub{})
        }){ root -> Scene(root, property("width"), property("height")) }
        stub(org.drx.evoleq.dsl.stub{})

    })
    stub(org.drx.evoleq.dsl.stub{evolve{
        while(!closeStage){
            delay(1)
        }
        //delay(100)
        /*
        fxRunTime{
            close()
        }

         */
        Parallel{it}
    }})
} }
